<?php

namespace JWT;
use Firebase\JWT\JWT;

class JWTWrapper
{
    public static function generate()
    {
        global $CFG;

        $token =
            [
                'iss' => 'Moodle',
                'aud' => 'LibEdu',
                'iat' => time(),
                'exp' => time() + $CFG->jwtExp,
                'nbf' => '2020-01-01',
            ];

        return JWT::encode($token, $CFG->jwtLibEdu);
    }

    public static function render()
    {
        return '<input type="hidden" name="jwtLibEdu" value="' . self::generate() . '" />';
    }

    public static function decode($jwt)
    {
        global $CFG;

        $decoded = JWT::decode($jwt, $CFG->jwtLibEdu, ['HS256']);
        self::validateJWT($decoded);

        if (!$decoded)
        {
            throw new \Exception('JWT authentication failed');
        }

        return $decoded;
    }

    public static function validateJWT($decoded)
    {
        $iatKey = 'iat';
        $expKey = 'exp';

        if (!array_key_exists($iatKey, $decoded))
        {
            throw new \Exception('IAT claim is required');
        }

        $nbf = $decoded->nbf;

        if ($decoded->$iatKey < $nbf)
        {
            throw new \Exception('JWT must be after ' . date('j M Y h:i A', $nbf));
        }

        if ((array_key_exists($expKey, $decoded) && $decoded->$expKey < time()) || (!array_key_exists($expKey, $decoded) && $decoded->$iatKey + config('jwt.exp_second') < time()))
        {
            throw new \Exception('JWT has expired');
        }
    }
}