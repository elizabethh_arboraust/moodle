<?php

class FormHelper
{
    public static function includeJS($src)
    {
        return '<script type="text/javascript" src="' . $src . '"></script>';
    }

    public static function runOnLoad($function)
    {
        return '
            <script>
                $(function()
                {
                    ' . $function . '
                });
            </script>';
    }
}