class user_edit
{
    static init()
    {
        user_edit.hideEmailField();
    }

    static hideEmailField()
    {
        $('#id_email').parents('.form-group').hide();

        setTimeout(function()
        {
            $('#id_email').parents('.form-group').remove();
        }, 1000);
    }
}